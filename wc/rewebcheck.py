# rewebcheck.py - This version reads a file containing the sites and
# terms to be monitored.  It supports logging and current state
# is stored in outboard JSON parm files
#
# 2018/05/29 - Making changes to better handle "requests" exception handling.
#    Requests module will now retry for 30 seconds.
#

# Import requests (to download the page)
from requests import ConnectionError
import requests
connection_timeout = 30

# Import BeautifulSoup (to parse what we download)
from bs4 import BeautifulSoup

import redatetime
import logging

# My PARM File Manager
import time
import os
os.chdir("/home/edwarr/wc/")
import reparms
import reemail


# Look for the term PASSOK
SITES = [
    {
        "url": "http://www.pslhost.com/webcheck.cfm",
        "searchfor": "PASSOK",
        "FNState": "F1.JSON"
    },
    {
        "url": "https://www.voterlink.ab.ca",
        "searchfor": "allow voters",
        "FNState": "F2.JSON"
    }
]

# Start Log
LOG_FILE='rewebcheck.log'
logging.basicConfig(filename=LOG_FILE, level=logging.INFO)
cXMsg = 'New Haggis Version - 2022-11-18'
logging.info( cXMsg )
print( cXMsg )


# Iterate over the SITES listing and test each url
for web in SITES:
    #print "---"
    #print "URL is set to:", web["url"]
    #print "Searchfor is set to:", web["searchfor"]
    #print "FN is set to:", web["FNState"]

    # Get the PARM JSON file ready
    cURL = web["url"]
    cTerm = web["searchfor"]
    cFN = web["FNState"]
    cDT = redatetime.reNowFormatted_YYYYMMDDHHMMSS()
    fnjson = reparms.ParmFileHandler(cFN)
    cLastStatus = fnjson.get_parm('STATUS')  # OK or BAD
    fnjson.set_parm('URL', cURL)
    fnjson.set_parm('TERM', cTerm)
    fnjson.set_parm('DATETIME', cDT)

    cPrintPrefix = cDT + " " + cURL

    # set the headers like we are a browser,
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

    # download the homepage
    start_time = time.time()
    while True:
        try:
            response = requests.get(cURL, headers=headers)
            break
        except ConnectionError:
            if time.time() > start_time + connection_timeout:
                print('Requests() failed.')
                logging.warning('Requests() failed.')
                raise Exception('Unable to get updates after {} seconds of ConnectionErrors'.format(connection_timeout))
            else:
                print( 'Warning: Requests() encountered possible network error.  Waiting 1 second.' )
                logging.warning('Warning: Requests() encountered possible network error.  Waiting 1 second.')
                time.sleep(1) # attempting once every second

    # parse the downloaded homepage and grab all text, then,
    soup = BeautifulSoup(response.text, "html.parser")

    # if the item is not found
    if str(soup).find(cTerm) == -1:
        print ( cPrintPrefix + " " + "-- BAD" )
        logging.info(cPrintPrefix + " " + "-- BAD")
        fnjson.set_parm('STATUS', 'BAD')
        fnjson.incr_parm('BAD_COUNT')
        # Send email
        if cLastStatus != 'BAD':
            reemail.send_email(
                'Error ' + cURL,
                'Time: ' + cDT + '\n' + \
                'Error encountered \n' + \
                'Server: ' + cURL
            )
            print ( 'Email sent' )
            logging.warning('Email sent')
        logging.warning('CHECK IS BAD')
    else:
        print ( cPrintPrefix + " " + "-- OK" )
        logging.info(cPrintPrefix + " " + "-- OK")
        fnjson.set_parm('STATUS', 'OK')
        fnjson.incr_parm('OK_COUNT')
        # Send email
        if cLastStatus != 'OK':
            reemail.send_email(
                'OK ' + cURL,
                'Time: ' + cDT + '\n' + \
                'Status OK\n' + \
                'Server: ' + cURL
            )
            print ( 'Email sent' )
            logging.warning('Email sent')
    fnjson.write_file()
