# reemail.py
# Email is supported via www.mailgun.com
# There are two ways to send email, the API and the SMTP way.  I wasn't
# able to get the API way to work for me so I'm doing this SMTP.
#
import requests
import smtplib
from email.mime.text import MIMEText


def send_email(cSubject, cMsg):
    send_simple_message_SMTP(cSubject, cMsg)


def send_simple_message():
    return requests.post(
        "https://api.mailgun.net/v3/mg.pslweb.ca",
        auth=("api", "key-711a7ef32588cf8053eeb49a08feb270"),
        data={"from": "noreply@mg.pslweb.ca",
              "to": "Randy <randy.edwards@shaw.ca>",
              "subject": "Hello Randy",
              "text": "Congratulations Randy here you go."})


def send_simple_message_SMTP(cSubject, cMsg):
    msg = MIMEText(cMsg)
    msg['Subject'] = cSubject
    msg['From']    = "noreply@mg.pslweb.ca"
    msg['To']      = "randy.edwards@professional-systems.com"
#    msg['To']      = "randy.edwards@shaw.ca"

    s = smtplib.SMTP('smtp.mailgun.org', 587)

    s.login('postmaster@mg.pslweb.ca', '821314d4c2bc643fd3839687f14fb2e5')
    s.sendmail(msg['From'], msg['To'], msg.as_string())
    s.quit()
