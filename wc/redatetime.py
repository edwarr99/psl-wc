# redatetime.py
# This file is running in the UK somewhere.  I am not absolutely sure what the
# timezone is.  But I am sure that I want this job to report date / times in
# the MST or MDT.  Therefore, this module is going to become responsible for
# handling date concerns now.
from datetime import datetime
from pytz import timezone

def reNow():
    tzmountain = timezone('Canada/Mountain')
    return datetime.now(tzmountain)


def reNowFormatted_YYYYMMDDHHMMSS():
    return reNow().strftime("%Y-%m-%d %H:%M:%S")
