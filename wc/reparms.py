# reparms.py
# Parameters are stored in a JSON-like text file.  Read_file is
# attempted on instatiation.  Write_file must be done manually.
import json
import os.path


# Try get / setter again


class ParmFileHandler():

    def __init__(self, cFN='test.json'):
        self.parms = {}    # creates a new empty dict
        self.FNParms = cFN
        self.read_file()   # does nothing if file does not exist

    def set_parm(self, cKey, dictValue):
        self.parms[cKey] = dictValue

    def incr_parm(self, cKey):
        nInt = self.get_parm_asint(cKey) + 1
        self.parms[cKey] = nInt
        return nInt

    def get_parm(self, cKey):
        # Return empty string if not found
        return self.parms.get(cKey, "")

    def get_parm_asint(self, cKey):
        # Return empty string if not found
        return self.parms.get(cKey, 0)

    def read_file(self):
        # Check that file exists
        if os.path.isfile(self.FNParms):
            # Open the file
            in_file = open(self.FNParms, 'r')

            # Load the contents from the file, which creates a new dictionary
            self.parms = json.load(in_file)

            # Close the file
            in_file.close()

    def write_file(self):
        # Open the file for writing
        out_file = open(self.FNParms, 'w')

        # Save the dictionary to this filename
        json.dump(self.parms, out_file, indent=4)

        out_file.close()
