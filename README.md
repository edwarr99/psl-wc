# PSL - WC (PSL Web Check) #
## What's in this GIT? ##
This GIT contains the production code for the Professional Systems web check.

This is currently configured on PythonAnywhere.  It runs 3 times per hour presently.  If an issue is encountered it sends an email to randy.edwards@professional-systems.com.


## FTP 
You can access the pythonAnywhere environment by SFTP.  Currently 
I have FileZilla installed on the Mac for this purpose.


### Comments ###
This project started 10/5/2017.

* 2022-11-18
Needed to convert the site to move away from a non-standard
"dangermouse" environment.  Everything seems to be up-to-date
in the GIT.  I just changed a bunch of print statements in 
rewebcheck.py to include brackets.  That seemed to do the trick
for this.

